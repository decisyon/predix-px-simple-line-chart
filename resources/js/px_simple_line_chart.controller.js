(function() {
	'use strict';

	/*
	* Controller to manage the Simple Line chart widget.
	* link:https://www.predix-ui.com/?show=px-simple-line-chart&type=component
	*/
	function PxSimplelineChartCtrl($scope, $timeout, $element){
		var ctrl								= this;
		var	target								= $scope.DECISYON.target;
		var	ctx									= target.content.ctx;
		var	refObjId							= ctx.refObjId.value;
		var INVALID_NUMBER_VALUE_PARAM_VALUE	= -1;
		var	EMPTY_PARAM_VALUE					= 'PARAM_VALUE_NOT_FOUND';
		var	BLANK_PARAM_VALUE					= '';
		var	unbindWatches						= [];
		var	OVERFLOW_BUG_FIX					= 2;

		/* Normalize value to set on web component by property value*/
		var getNormalizePropertyValue = function(property) {
			var valueToReturn = property.value == EMPTY_PARAM_VALUE || angular.equals(property.value, INVALID_NUMBER_VALUE_PARAM_VALUE) ? BLANK_PARAM_VALUE : property.value;

			switch (property.id) {
				case '$pxMinLabel': {
					//Manage a min label to show when it is not set
					valueToReturn = angular.equals(valueToReturn, BLANK_PARAM_VALUE) ? 'Min' : property.value;
					break;
				}
				case '$pxMaxLabel': {
					//Manage a max label to show when it is not set
					valueToReturn = angular.equals(valueToReturn, BLANK_PARAM_VALUE) ? 'Max' : property.value;
					break;
				}
			}

			return valueToReturn;

		};

		/* Managing of line chart Properties */
		var manageParamsFromContext = function(context) {
			//STRING PROPERTIES
			ctrl.minLabel			= getNormalizePropertyValue(context.$pxMinLabel);
			ctrl.maxLabel			= getNormalizePropertyValue(context.$pxMaxLabel);
			ctrl.thresholdLabel		= getNormalizePropertyValue(context.$pxThresholdLabel);
			//NUMERIC PROPERTIES
			ctrl.min				= getNormalizePropertyValue(context.$pxMin);
			ctrl.max				= getNormalizePropertyValue(context.$pxMax);
			ctrl.threshold			= getNormalizePropertyValue(context.$pxThreshold);
			ctrl.columns			= getNormalizePropertyValue(ctx.$pxColumns);
			ctrl.rows				= getNormalizePropertyValue(ctx.$pxRows);
			//HARD-CODED PROPERTIES
			ctrl.refObjId           = refObjId;
		};

		/* Inizialize chart*/
		var inizializeChart = function(data, ctx) {
			manageParamsFromContext(ctx);
			attachListeners(ctx);
			applyDimensionsToWebComponent(ctx);
			setData(data);
		};

		/* Update chart for context parameters and dimesion*/
		var updateChart = function(data, ctx) {
			manageParamsFromContext(ctx);
			applyDimensionsToWebComponent(ctx);
			//Force re-drawing for predix web component
			document.getElementById(refObjId)._drawChart();
		};

		/* Populate the data fetched through controller */
		var setData = function(data) {
			ctrl.lineData = data;
			ctrl.currentTpl = 'simpleLineChart.html';
		};

		/* Routine called when angular element is destroied*/
		var destroyWidget = function() {
			for (var i = 0; i < unbindWatches.length; i++){
				unbindWatches[i]();
			}
		};

		/* Function to handle DC failure */
		var setChartInError = function(rejection) {
			ctrl.errorMsg = rejection.errorMsg;
			ctrl.currentTpl = 'error.html';
			destroyWidget();
		};

		/* Get data from the Connector */
		var getData = function(ctx) {
			//The widget is in dashboard; prevent abnormal behavior into preview
			if (target.page){
				if (angular.equals(ctx.useDataConnector.value,'true')){
					var dc = target.getDataConnector();
					dc.ready(function(dataConnector) {
						dataConnector.onUpdate(function(data) {
							dataConnector.instance.data.then(function success(data) {
								updateChart(data, ctx);
							},function error(rejection) {
								setChartInError(rejection);
							});
						});
						dataConnector.instance.data.then(function success(data) {
							inizializeChart(data, ctx);
						},function error(rejection) {
							setChartInError(rejection);
						});
					});
				} else {
					ctrl.lineData  = [];
				}
			}else {
				setChartInError({errorMsg : 'You are not on mashboard page. The widget can\'t be inizialized.'});
			}
		};
		/* It adjust size ad fix overflow*/
		var getAdjustedSizing = function(size) {
			var adjustedSize = size.replace(/%/g, '').trim();
			return (parseInt(adjustedSize) > OVERFLOW_BUG_FIX) ? parseInt(size) - OVERFLOW_BUG_FIX;
		};

		/* 	Set the container widget size.If you set both this and height to "auto", the chart will expand to fill its containing element.
			Note: The parent containing element must be a block-level element or have a defined width/height so that the component can inherit the value.
		*/
		var applyDimensionsToWebComponent = function(ctx) {
			ctrl.width    = getAdjustedSizing(ctx['min-width'].value);
			ctrl.height   = getAdjustedSizing(ctx['min-height'].value);
		};

		/* Watch on widget context to observe parameter changing after loading */
		var attachListeners = function() {
			unbindWatches.push($scope.$watch('DECISYON.target.content.ctx', function(newCtx, oldCtx) {
				if (!angular.equals(newCtx, oldCtx)){
					updateChart(ctrl.lineData, newCtx);
				}
			}, true));
			//Listener on destroy angular element; in this case we destroy all watch and listener
			$scope.$on('$destroy', destroyWidget);
		};

		//First instruction to get the data
		getData(ctx);
	}
	PxSimplelineChartCtrl.$inject = ['$scope', '$timeout', '$element'];

	DECISYON.ng.register.controller('pxSimplelineChartCtrl', PxSimplelineChartCtrl);
}());